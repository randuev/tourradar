#!/bin/bash

subject="List of logs from fpm"

if [ -f /home/tr/logs/app.log ]; then
    nontr=$(awk -F\" '{if ($4!="tr")print}' /home/tr/logs/app.log)
fi

if [ -z "$nontr" ]; then subject="Wrong user in app.log from fpm"; fi

echo "List of files in logs directory:" $(ls -1 /home/tr/logs) |  mail -s "$subject" SENDLOGSTO
