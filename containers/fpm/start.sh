#!/bin/bash

# setup logrotate
sed -i "s/LOGDAYS/$LOGDAYS/g" /home/tr/logrotate.conf

# setup mail
sed -i "s/SENDLOGSTO/$SENDLOGSTO/g" /home/tr/sendlogs.sh

# start everything
/usr/bin/supervisord -c /etc/supervisord.conf
