#!/bin/bash

# setup mail
echo "tr: $FORWARDMAILTO" > /etc/aliases
/usr/sbin/newaliases

# setup mysql access
sed -i "s/MYSQL_USER/$MYSQL_USER/" /usr/local/apache2/conf/httpd.conf
sed -i "s/MYSQL_PASSWORD/$MYSQL_PASSWORD/" /usr/local/apache2/conf/httpd.conf
sed -i "s/MYSQL_DATABASE/$MYSQL_DATABASE/" /usr/local/apache2/conf/httpd.conf

# start everything
/usr/bin/supervisord -c /etc/supervisord.conf
